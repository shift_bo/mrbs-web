import axios from 'axios'

export const baseUrl =
  process.env.NODE_ENV === 'production'
    ? 'http://admin.oaloft.com'
    : 'http://localhost:5000'

const request = axios.create({
  baseURL: baseUrl + '/api/v1/web',
  timeout: 6000
})

export default request
