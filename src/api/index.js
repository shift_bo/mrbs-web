import request from './request'
/*
    获取区域列表1

*/
export const getArealist = () => {
  return request({
    url: '/areas'
  })
}
export const getArealistAdmin = () => {
  return request({
    url: '/areas/getareas'
  })
}
export const getAreaRoom = ({ roomId }) => {
  return request({
    url: `/areas/rooms/s?area_id=${roomId}`,
    method: 'get'
  })
}

export const addArealistRooms = data => {
  return request({
    url: '/areas/rooms/s',
    method: 'post',
    data
  })
}

export const delRoom = data => {
  return request({
    url: '/areas/rooms/del',
    method: 'post',
    data
  })
}
export const changeRoomInfo = data => {
  return request({
    url: '/areas/rooms/changeInfo',
    method: 'post',
    data
  })
}

export const changeRoomState = data => {
  return request({
    url: '/areas/rooms/changeState',
    method: 'post',
    data
  })
}

export const changeAreaInfo = data => {
  return request({
    url: '/areas/rooms/changeAreaInfo',
    method: 'post',
    data
  })
}

export const addAreaInfo = data => {
  return request({
    url: '/areas/rooms/addAreaInfo',
    method: 'post',
    data
  })
}

export const getDevice = () => {
  return request({
    url: '/areas/rooms/device'
  })
}

export const addDevice = data => {
  return request({
    url: '/areas/rooms/adddevice',
    method: 'post',
    data
  })
}
export const delDevice = data => {
  return request({
    url: '/areas/rooms/deldevice',
    method: 'post',
    data
  })
}

export const changeDeviceName = data => {
  return request({
    url: '/areas/rooms/changedevicename',
    method: 'post',
    data
  })
}
export const getUserInfo = () => {
  return request({
    url: '/employees/userInfo'
  })
}

export const addUserInfo = data => {
  return request({
    url: '/employees/adduserInfo',
    method: 'post',
    data
  })
}
export const ChangeUserInfo = data => {
  return request({
    url: '/employees/changeuserInfo',
    method: 'post',
    data
  })
}
export const DelUserInfo = data => {
  return request({
    url: '/employees/deluserInfo',
    method: 'post',
    data
  })
}

export const getUser = () => {
  return request({
    url: '/currentuser'
  })
}

export const getRole = () => {
  return request({
    url: '/employees/Role'
  })
}
export const queryRole = data => {
  return request({
    url: '/employees/Role',
    method: 'post',
    data
  })
}
export const syncUpdate = () => {
  return request({
    url: '/userlist/update'
  })
}
