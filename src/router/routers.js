import Main from '@/components/main'
// import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面不会缓存
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */

export default [
  {
    path: '/manage',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          hideInMenu: true,
          title: '空白页',
          notCache: true,
          icon: 'ios-albums-outline'
        },
        component: () => import('@/view/single-page/home/home.vue')
      }
    ]
  },
  {
    path: '/multilevel',
    name: 'multilevel',
    meta: {
      icon: 'md-menu',
      title: '会议室系统'
    },
    component: Main,
    children: [
      {
        path: 'area_room',
        name: 'area_room',
        meta: {
          icon: 'ios-briefcase',
          title: '会议室管理'
        },
        component: () => import('@/view/multilevel/area_room.vue')
      },
      {
        path: 'area',
        name: 'area',
        meta: {
          icon: 'ios-create-outline',
          title: '区域管理'
        },
        component: () => import('@/view/multilevel/area.vue')
      },
      {
        path: 'device',
        name: 'device',
        meta: {
          icon: 'md-trending-up',
          title: '设备管理'
        },
        component: () => import('@/view/multilevel/device.vue')
      },
      {
        path: 'people_list',
        name: 'people_list',
        meta: {
          icon: 'ios-options',
          title: '人员管理'
        }, // <Icon type="ios-options" />
        component: () => import('@/view/multilevel/people_list.vue')
      },
      {
        path: 'power',
        name: 'power',
        meta: {
          icon: 'ios-options',
          title: '权限管理'
        }, // <Icon type="ios-options" />
        component: () => import('@/view/multilevel/power.vue')
      }
    ]
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/404.vue')
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  }
]
