const getters = {
  areaList: state => state.table.areaList,
  user: state => state.user.user
}

export default getters
