import { getArealist } from '@/api/index'
import { getAreaRoom } from '../../api'

const store = {
  state: {
    areaList: [], // 区域列表
    areaRoomList: []
  },
  mutations: {
    SET_AREA_LIST (state, areaList) {
      // 设置区域列表
      state.areaList = areaList
    },
    SET_AREA_ROOM_LIST (state, areaRoomList) {
      // 设置区域房间列表
      state.areaRoomList = areaRoomList
    }

  },
  actions: {
    ActionGetAreaList ({ commit }) {
      return new Promise(resolve => {
        getArealist().then(resp => {
          if (resp.status === 200) {
            const data = resp.data
            commit('SET_AREA_LIST', data.areaList)
            resolve(data)
          }
        })
      })
    },
    ActionGetAreaRoom ({ commit }) {
      return new Promise(resolve => {
        getAreaRoom({ roomId: 2 }).then(resp => {
          if (resp.status === 200) {
            const data = resp.data
            commit('SET_AREA_ROOM', data.areaRoom)
            resolve(data)
          }
        })
      })
    }
  }
}
export default store
