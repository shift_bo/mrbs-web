import { getUserInfo } from '@/api/user'
import { setRoleId } from '@/libs/util'
import { getUser } from '../../api'
import { queryRole } from '@/api/index'

export default {
  state: {
    userName: '',
    userId: '',
    avatorImgPath: '',
    token: '',
    access: '',
    hasGetInfo: false,
    roleName: ''
  },
  mutations: {
    setAvator (state, avatorPath) {
      state.avatorImgPath = avatorPath
    },
    setUserId (state, id) {
      state.userId = id
    },
    setUserName (state, name) {
      state.userName = name
    },
    setAccess (state, access) {
      state.access = access
    },
    setToken (state, token) {
      state.token = token
    },
    setHasGetInfo (state, status) {
      state.hasGetInfo = status
    },
    setRoleName (state, roleName) {
      state.roleName = roleName
    }
  },
  actions: {
    // 获取token
    // 获取用户相关信息
    getUserInfo ({ state, commit }) {
      return new Promise((resolve, reject) => {
        getUserInfo(state.token)
          .then(res => {
            const data = res.data
            commit('setUserName', data.name)
            commit('setUserId', data.user_id)
            commit('setAccess', data.access)
            commit('setHasGetInfo', true)
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    getUserRoleId ({ state, commit }) {
      return new Promise((resolve, reject) => {
        getUser()
          .then(resp => {
            commit('setAvator', resp.data.data.currentUser.avatar)
            commit('setRoleName', resp.data.data.currentUser.role_name)
            resolve(resp.data.data)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}
