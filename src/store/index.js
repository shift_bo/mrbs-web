import Vue from 'vue'
import Vuex from 'vuex'

import user from './module/user'
import app from './module/app'
import table from './module/table'
import getters from './getters'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    user,
    app,
    table
  },
  getters
})
